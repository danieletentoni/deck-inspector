import { describe, expect, it } from 'vitest';
import { countCards, groupCards } from '../card';

describe('Card', () => {
  it.each([
    { cards: [{ count: 1, name: 'c' }], result: { c: 1 } },
    {
      cards: [
        { count: 1, name: 'c' },
        { count: 1, name: 'c' },
      ],
      result: { c: 2 },
    },
    {
      cards: [
        { count: 1, name: 'c' },
        { count: 1, name: 'c' },
        { count: 1, name: 'd' },
        { count: 1, name: 'd' },
      ],
      result: { c: 2, d: 2 },
    },
  ])('group cards', ({ cards, result }) => {
    expect(groupCards(cards)).to.be.deep.equal(result);
  });

  it.each([
    { cards: [{ count: 1, name: 'c' }], result: 1 },
    {
      cards: [
        { count: 1, name: 'c' },
        { count: 1, name: 'c' },
      ],
      result: 2,
    },
    {
      cards: [
        { count: 1, name: 'c' },
        { count: 1, name: 'c' },
        { count: 1, name: 'd' },
        { count: 1, name: 'd' },
      ],
      result: 4,
    },
  ])('count cards', ({ cards, result }) => {
    expect(countCards(cards)).to.be.equal(result);
  });
});
