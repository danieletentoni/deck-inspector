import type { Card } from './card';

export interface Deck {
  title: string;
  cards: Card[];
}
