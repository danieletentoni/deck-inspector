export function groupCards(cards: Card[]) {
  const groups: CardCounter = {};
  cards.forEach((elem) => {
    if (elem.name in groups) {
      groups[elem.name] += elem.count;
    } else {
      groups[elem.name] = elem.count;
    }
  });

  return groups;
}

export function countCards(cards: Card[]) {
  const groups = groupCards(cards);
  return Object.entries(groups).reduce((prev, curr) => prev + curr[1], 0);
}

export interface Card {
  count: number;
  name: string;
}

export type CardCounter = { [key: string]: number };
