import { createRouter, createWebHistory } from 'vue-router';
import Home from '@/views/HomeView.vue';
import CollectionView from '@/views/CollectionView.vue';
import DeckView from '@/views/DeckView.vue';

const routes = [
  { path: '/', component: Home },
  {
    path: '/collection',
    component: CollectionView,
  },
  {
    path: '/deck',
    component: DeckView,
  },
];

const router = createRouter({
  // 4. Provide the history implementation to use. We are using the hash history for simplicity here.
  history: createWebHistory(),
  routes, // short for `routes: routes`
});

export default router;
