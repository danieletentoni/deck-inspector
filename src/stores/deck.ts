import type { Deck } from '@/models/deck';
import { defineStore } from 'pinia';
import { ref } from 'vue';

export const useDeckStore = defineStore('deck', () => {
  const decks = ref<Deck[]>([]);

  function add(deck: Deck) {
    decks.value.push(deck);
  }

  function loadDecks() {
    const str = localStorage.getItem('piniaState');
    if (str) {
      const d = JSON.parse(str);
      decks.value = d.deck.decks;
    }
  }

  return { decks, add, loadDecks };
});
