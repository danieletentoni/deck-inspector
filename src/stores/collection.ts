import { defineStore } from 'pinia';
import { computed, ref } from 'vue';
import * as d3 from 'd3';
import { useDeckStore } from './deck';
import { type Card, groupCards } from '@/models/card';

export interface Binder {
  title: string;
  cards: CardList;
}

export const localStorageKey = 'collection';

export type CardList = string | Card[];

export function cardListToArray(cardList: CardList): Card[] {
  if (typeof cardList === 'string') {
    const d3Array = d3.csvParse(cardList);
    return d3Array
      .map((m) => ({ count: Number(m.Count), name: String(m.Name).trim() }) as Card)
      .filter((f) => f.name);
  } else if (cardList.length) {
    return cardList as Card[];
  }
  return [];
}

export const useCollectionStore = defineStore('collection', () => {
  const collection = ref<Card[]>([]);

  const freeCollection = computed(() => {
    const decks = useDeckStore();
    const usedCards = decks.decks.flatMap((c) => c.cards);
    const group = groupCards(usedCards);
    const availableCards = Object.entries(groupCards(collection.value));
    const free = availableCards
      .map((m) => {
        const c = group[m[0]] ?? 0;
        return { count: m[1] - c, name: m[0] } as Card;
      })
      .filter((f) => f.count > 0);
    return free;
  });

  function loadCollection() {
    const str = localStorage.getItem('piniaState');
    if (str) {
      const col = JSON.parse(str);
      collection.value = col.collection.collection;
    }
  }

  function fillDecks(deck: CardList) {
    const cardList: Card[] = cardListToArray(deck);
    const deckCards = groupCards(cardList);
    const collectionCards = groupCards(collection.value);
    const availableCards = Object.entries(collectionCards).filter((f) => deckCards[f[0]] <= f[1]);
    return availableCards.length >= Object.entries(deckCards).length;
  }

  function uploadCollection(cards: CardList) {
    const cardList: Card[] = cardListToArray(cards);
    collection.value = cardList;
  }

  return {
    collection,
    fillDecks,
    freeCollection,
    loadCollection,
    uploadCollection,
  };
});
