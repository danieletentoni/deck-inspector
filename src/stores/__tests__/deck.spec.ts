import { setActivePinia, createPinia } from 'pinia';
import { beforeEach, describe, expect, it } from 'vitest';
import { useDeckStore } from '../deck';

describe('Deck store', () => {
  beforeEach(() => {
    setActivePinia(createPinia());
  });

  it('Add a new deck', () => {
    const deckStore = useDeckStore();
    expect(deckStore.decks).to.have.length.at.most(0);
    deckStore.add({ title: 'new', cards: [] });
    expect(deckStore.decks).to.have.length.at.least(1);
  });
});
