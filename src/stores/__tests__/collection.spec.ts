import { setActivePinia, createPinia } from 'pinia';
import { beforeEach, describe, expect, it } from 'vitest';
import { useCollectionStore } from '../collection';
import { useDeckStore } from '../deck';

describe('Collection', () => {
  beforeEach(() => {
    setActivePinia(createPinia());
  });

  it.each([
    { cardList: [{ name: 'card', count: 1 }], length: 1 },
    {
      cardList: `Name,Count
    _____ Goblin,4
    `,
      length: 1,
    },
    {
      cardList: [
        { name: 'card', count: 1 },
        { name: 'd', count: 1 },
      ],
      length: 2,
    },
    {
      cardList: `Name,Count
    _____ Goblin,4
    Quel Goblino,4
    `,
      length: 1,
    },
  ])('could load from a file', ({ cardList, length }) => {
    const store = useCollectionStore();
    expect(store.collection).to.have.length.at.most(0);
    store.uploadCollection(cardList);
    expect(store.collection).to.have.length.at.least(length);
  });

  it('has free collection property', () => {
    const store = useCollectionStore();
    store.uploadCollection([{ name: 'card', count: 1 }]);
    expect(store.collection).to.have.length(1);
    expect(store.freeCollection).to.have.length(1);
    store.uploadCollection([
      { name: 'card', count: 1 },
      { name: 'card', count: 1 },
    ]);
    expect(store.collection).to.have.length(2);
    expect(store.freeCollection).to.be.deep.equal([{ name: 'card', count: 2 }]);
  });

  it('try to fill a deck', () => {
    const store = useCollectionStore();
    const decks = useDeckStore();
    store.uploadCollection([
      { name: 'card', count: 1 },
      { name: 'card', count: 1 },
    ]);
    decks.add({ title: 'd', cards: [{ name: 'card', count: 1 }] });
    expect(store.freeCollection).to.be.deep.equal([{ name: 'card', count: 1 }]);
  });

  it('use all cards in a deck', () => {
    const store = useCollectionStore();
    const decks = useDeckStore();
    store.uploadCollection([
      { name: 'card', count: 1 },
      { name: 'card', count: 1 },
    ]);
    decks.add({ title: 'd', cards: [{ name: 'card', count: 2 }] });
    expect(store.freeCollection).to.have.length.at.most(0);
  });

  it('use more cards than holding in a deck', () => {
    const store = useCollectionStore();
    const decks = useDeckStore();
    store.uploadCollection([
      { name: 'card', count: 1 },
      { name: 'card', count: 1 },
    ]);
    decks.add({ title: 'd', cards: [{ name: 'card', count: 3 }] });
    expect(store.freeCollection).to.have.length.at.most(0);
  });

  it('use different cards', () => {
    const store = useCollectionStore();
    const decks = useDeckStore();
    store.uploadCollection([
      { name: 'a', count: 2 },
      { name: 'b', count: 3 },
      { name: 'c', count: 4 },
      { name: 'd', count: 5 },
      { name: 'e', count: 6 },
    ]);
    expect(store.freeCollection).to.have.length(5);
    decks.add({ title: 'd', cards: [{ name: 'a', count: 2 }] });
    expect(store.freeCollection).to.have.length(4);
    decks.add({ title: 'e', cards: [{ name: 'a', count: 2 }] });
    expect(store.freeCollection).to.have.length(4);
    decks.add({ title: 'f', cards: [{ name: 'b', count: 2 }] });
    expect(store.freeCollection).to.have.length(4);
    decks.add({ title: 'g', cards: [{ name: 'b', count: 2 }] });
    expect(store.freeCollection).to.have.length(3);
  });

  it('fill decks', () => {
    const store = useCollectionStore();
    const res = store.fillDecks([{ name: 'b', count: 2 }]);
    expect(res).to.be.false;
    store.uploadCollection([{ name: 'b', count: 2 }]);
    const res2 = store.fillDecks([{ name: 'b', count: 2 }]);
    expect(res2).to.be.true;
  });

  it('fill decks with a string', () => {
    const store = useCollectionStore();
    const res = store.fillDecks(`Name,Count
    _____ Goblin,4
    `);
    expect(res).to.be.false;
    const cardstring = `Name,Count
    _____ Goblin,4
    Abrade,4
    `;
    const deckstring = `Name,Count
    _____ Goblin,4
    `;
    store.uploadCollection(cardstring);
    /*const cardList = cardListToArray(deckstring)
    const deckCards = groupCards(cardList)
    const collectionCards = groupCards(store.collection)
    const availableCards = Object.entries(collectionCards).filter(
      (f) => deckCards[f[0]] <= f[1]
    )*/
    const t = store.fillDecks(deckstring);
    expect(t).toBeTruthy();
  });
});
